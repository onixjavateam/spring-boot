# Spring Boot
Эта статья ориентирована на новичков в Java разработке, которые не использовали Spring Boot. Принято считать, что сложно
разрабатывать на Java: высокий порог вхождения в технологии Java, если сравнивать с PHP, Node.js, например.
Spring Boot ломает эти стереотипы и позволяет разработчикам сконцентрироваться на бизнес логике, а не на конфигурации
приложения. В статье рассмотрим основные возможности Spring Boot и напишем небольшое приложение.

## Что такое Spring?
Если очень кратко, то Spring - это универсальный framework под JVM, при чем разрабатывать можно не только на Java, но и
на Kotlin, и на Groovy. Но в широком смысле под термином Spring часто подразумевают целое семейство проектов. Главным
идейным лидером и ведущим программистом Spring является [Juergen Hoeller](https://spring.io/team/jhoeller).
Вот список нескольких ключевых проектов Spring: 
* Spring Framework (или Spring Core) - реализованы базовые вещи, Dependency Injection, MVC, events, AOP и т. д.
* Spring Data - доступ к данным: реляционные и нереляционные БД, KV хранилища и т. д.
* Spring Cloud - много полезного для микро-сервисной архитектуры. Например service discovery, трасировка и диагностика,
балансировщики запросов, configuration servers.
* Spring Security - авторизация и аутентификация, OAuth, LDAP.
* Spring Session - для всего, что связано с Http Session, WebSocket Session и т. д.

Весь список проектов вы можете посмотреть [здесь](https://spring.io/projects).

## Spring Boot
И наконец Spring Boot - это наверное самый интересный и самый важный проект Spring экосистемы. Какую проблему решает
Spring Boot? Spring framework сам по себе - достаточно сложный. При разработке новых приложений, вам каждый раз придется
делать типовые, а порой и не совсем простые вещи. Spring Boot максимально упрощает создание приложений, скрывая за собой
много рутинной работы. Забавная получается вещь: сообщество решило создать для фреймворка Spring свой собственный
фреймворк - Spring Boot.
Чем полезен Spring Boot:
* позволяет быстро создать и настроить приложение, запаковать его в самодостаточный исполняемый артефакт, который без
всяких условий будет готов к запуску. 
* Spring Boot не использует XML для конфигурации. Все настраивается через аннотации. 
* Простота управления зависимостями c помощью starters
* Используются автоконфигурации по максимуму.
* [Convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration). Для большинства
конфигураций не нужно ничего настраивать.
* Встроенный Web сервер. По умолчанию Embedded Apache Tomcat.

## Как создать Spring Boot приложение?
Есть два быстрых варианта:
* [Spring Initializr](https://start.spring.io). Через эту очень простую и удобную форму есть возможность сгенерировать
базовое приложение.
* С помощью IntelliJ IDEA. File -> New -> Project -> Spring Initializr. Причем IntelliJ IDEA напрямую использует
[Spring Initializr](https://start.spring.io) API.

На форме [Spring Initializr](https://start.spring.io) выберем Maven в качестве сборщика, и Java в качестве языка.
Также добавим Spring Web зависимость потому что мы будем разрабатывать RESTful API приложение.

![spring-start-io](https://bitbucket.org/onixjavateam/spring-boot/raw/master/paper-resources/spring-start-io.png)

Исходный код полученного приложения вы можете посмотреть [здесь](https://bitbucket.org/onixjavateam/spring-boot/src/master/).
    
## pom.xml и управление зависимостями
Давайте рассмотрим ближе из чего состоит сгенерированное приложение, начнем с конфигурации Maven - pom.xml. 
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.0.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <groupId>com.example</groupId>
    <artifactId>demo</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>demo</name>
    <description>Demo project for Spring Boot</description>
    <properties>
        <java.version>1.8</java.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>

```

### spring-boot-starter-parent
Это родительская конфигурация для нашего проекта, и там находится вся дефолтная конфигурация. Например огромное дерево
зависимостей с совместимыми версиями. Это весьма полезно, потому что разработчику теперь не нужно находить необходимые
версии для всех зависимостей. Также там есть необходимые плагины для билда и тд. В результате размер нашего pom.xml
сокращается в разы, как раз благодаря spring-boot-starter-parent.

### Зависимости spring-boot-starters
Чтобы процесс управления зависимостями сделать быстрее, Spring Boot неявно пакует необходимые сторонние зависимости
для каждого типа приложения в так называемые starter-пакеты.

У нас добавлено две зависимости, spring-boot-starter-web(добавили мы через форму [Spring Initializr](https://start.spring.io))
и spring-boot-starter-test(добавляется по умолчанию при создании приложения). В spring-boot-starter-web
инкапсулировано все что нужно для создания RESTful: spring-webmvc, jackson-json (для сериализации и десериализации json),
validation-api и embedded tomcat (мы поговорим чуть позже). В spring-boot-starter-test инкасуриваны: Spring Test, Junit,
Mockito и т.д.

Иными словами, Spring Boot с помощью таких вот стартеров собирает все общие зависимости в одном месте, что позволяет
разработчикам просто использовать их, а не добавлять все необходимые зависимости по отдельности.

### spring-boot-maven-plugin
В секции build у нас есть плагин spring-boot-maven-plugin который помогает нам собирать и запускать приложение. Так же
у плагина есть несколько очень полезных возможностей, например собирать executable jar, но мы это обсудим позже.

## Auto-configuration
Следующей превосходной возможностью Spring Boot является автоматическая конфигурация. После выбора необходимого
starter'а, Spring Boot попытается автоматически настроить приложение на основе добавленных вами jar-зависимостей.
Что это значит? Например, мы добавили spring-boot-starter-web. На основе этой зависимости Spring Boot автоматически
создаст и настроит такие объекты, как DispatcherServlet, MessageSource, ResourceHandlers и т. д.

Если же используется spring-boot-starter-data-jpa, Spring Boot автоматически создаст: DataSource, TransactionManager,
EntityManagerFactory, и считает настройки для подключения к базе данных из файла application.properties.
Если же настроек нету, то будут использованы настройки по умолчанию. Например в случае использования MySQL, приложение
будет пытаться создать коннект к localhost:3306. 

Важно отметить что автоматическая конфигурация может быть полностью переопределена.

## Встроенный Web Server
Классическая история создания Web приложений на Java следующая. Вначале мы скачиваем и устанавливаем Web/Application Server,
их там много разных, самые популярные: Apache Tomcat, Jetty, JBoss, GlassFish, IBM WebSphere, Oracle Weblogic.
Дальше собираем наше приложение в war и деплоим в этот Web Server. И таких приложений на один Web Server мы можем задеплоить
несколько.

![web-app-server](https://bitbucket.org/onixjavateam/spring-boot/raw/master/paper-resources/web-app-server.png)

Но почему Web/Application Server не сделать частью приложения? Мы бы таким образом сократили деплоймент на несколько
шагов, и мы бы могли настраивать Web/Application Server на уровне приложения. Все эти вопросы решает Spring Boot. Он
предоставляет Embedded Server, есть поддержка самых популярных Tomcat и Jetty.

![web-app-server](https://bitbucket.org/onixjavateam/spring-boot/raw/master/paper-resources/web-app-server.png)

Разработчикам таким образом не стоит беспокоиться о настройке отдельной от приложения сущности. Упрощается и стает более
прозрачным деплоймент. И самое важное приложение становится самодостаточным jar-файлом, который может запускаться независимо.
Создание таких самодостаточных web-приложений - не только удобно для разработки, но и отлично вписывается в микросервисную
архитектуру.

## Демо приложение
Давайте теперь посмотрим на наш ключевой класс DemoApplication, в нем есть функция main, которая и запускает наше
приложение. Так же есть магическая аннотация ```@SpringBootApplication```:
```java
@SpringBootApplication
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
```
Если посмотреть что внутри этой аннотации, то мы увидим несколько важных моментов:
 1. есть аннотация ```@EnableAutoConfiguration``` которая и говорит ```run()``` методу класса ```SpringApplication```
 запустить автоконфигурацию о которой мы говорили ранее.
 2. аннотация ```@ComponentScan``` сканирует весь ваш код в поисках контроллеров, компонентов, сервисов,
 репозиториев, дальше Spring создает их. Поэтому важно положить класс с аннотацией ```@SpringBootApplication```
 в корне вашего пакета.

Такое приложение с одним классом DemoApplication можно запускать, и оно запустится по умолчанию на порте 8080. Но так как
мы не реализовали ни одного контроллера и экшена то мы получим 404.

Но давайте сделаем немного больше. Задача тривиальная - RESTful CRUD приложение по управлению пользователями.
Начнем с модели пользователя, где всего три поля: id, name, age + геттеры/сеттеры:
```java
class User {
    private Integer id;
    private String name;
    private Integer age;
    Integer getId() {
        return id;
    }
    void setId(Integer id) {
        this.id = id;
    }
    String getName() {
        return name;
    }
    void setName(String name) {
        this.name = name;
    }
    Integer getAge() {
        return age;
    }
    void setAge(Integer age) {
        this.age = age;
    }
}
```
Дальше пишем сервис с нашей бизнес логикой. Не забываем добавлять аннотацию @Service:
```java
@Service
public final class UserService {
    private final List<User> users = new ArrayList<>();
    List<User> list() {
        return this.users;
    }
    Optional<User> get(final Integer id) {
        return this.users.stream()
                .filter(u -> id.equals(u.getId()))
                .findFirst();
    }
    User add(final User user) {
        user.setId(this.users.size() + 1);
        this.users.add(user);
        return user;
    }
    boolean delete(final Integer id) {
        final Optional<User> userOptional = this.get(id);
        if (userOptional.isPresent()) {
            this.users.remove(userOptional.get());
            return true;
        }
        return false;
    }
    boolean edit(final Integer id, final User user) {
        final Optional<User> userOptional = this.get(id);
        if (userOptional.isPresent()) {
            userOptional.get().setAge(user.getAge());
            userOptional.get().setName(user.getName());
            return true;
        }
        return false;
    }
}
```
И наконец контроллер:
```java
@RestController
@RequestMapping("user")
public final class UserController {
    private final UserService userService;
    public UserController(final UserService userService) {
        this.userService = userService;
    }
    @GetMapping
    public ResponseEntity<List<User>> getUsers() {
        return new ResponseEntity<>(this.userService.list(), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable final Integer id) {
        return this.userService.get(id)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    @PostMapping
    public ResponseEntity<User> addUser(@RequestBody final User user) {
        return new ResponseEntity<>(this.userService.add(user), HttpStatus.OK);
    }
    @PutMapping("/{id}")
    public ResponseEntity<User> editUser(@PathVariable final Integer id, @RequestBody final User user) {
        if (this.userService.edit(id, user)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable final Integer id) {
        if (this.userService.delete(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
```
Обращаем особое внимание на конструктор контроллера: Dependency Injection через конструктор в действии. Spring автоматически
будет инжектить объект сервиса в контроллер, нам достаточно только огласить конструктор.

## Maven wrapper
Если мы посмотрим на структуру нашего приложения, то мы увидим файлы mvnw, mvnw.bat и директорию .mvn. Генератор 
strart.spring.io добавляет в наш проект по умолчанию так называемый maven wrapper, что это такое? С помощью него не нужно
устанавливать Maven глобально. Также можно зафиксировать конкретную версию Maven для проекта, в некоторых случаях это
может быть полезно.

## Как запустить?
В этом нам и поможет maven wrapper:
```shell script
mvnw spring-boot:run
```
причем ```spring-boot:run``` - это как раз комманда, которую нам предоставляет ```spring-boot-maven-plugin```.

Для продакшена нам нужно сбилдить наш код, тут нам тоже поможет maven wrapper, выполняем:
```shell script
mvnw package
```
В результате появляется директория target, где нас интересуют два файла - .jar и .jar.original. .jar - это так называемый
fat jar, куда помимо ваших классов, включены все зависимости в том числе фреймворк и embedded tomcat. Это самодостаточный
артефакт который можно запускать:
```shell script
java -jar target/demo-0.0.1-SNAPSHOT.jar
```
А вот .jar.original - это артефакт с исключительно вашими классами.

## Executable jar
Очень интересной возможностью является так называемый executable jar. Если дописать в настройки плагина
```spring-boot-maven-plugin``` configuration.executable = true:
```xml
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <configuration>
        <executable>true</executable>
    </configuration>
</plugin>
```
то мы получим jar который можно запускать без ```java -jar```. Если открыть в редакторе этот jar, то там в начале файла можно
увидеть bash скрипт, где реализованы команды start, stop, restart. Этот jar можно запускать как сервис(демон) с помощью
System V, Systemd, init.d... Также можно использовать docker и запускать в нем.

## Заключение
Очень не просто уместить информацию о таком проекте как Spring Boot в рамках одной статьи. Очень много есть
нюансов и полезных возможностей. Но в тоже время эта статья может быть неплохим стартом.

Spring Framework обладает подробной и хорошо структурированной документацией, которую стоит изучать. Есть большое и
отзывчивое сообщество, и самое главное есть чистые исходники которые вполне могут заменить даже документацию.
