package com.example.demo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public final class UserService {
    private final List<User> users = new ArrayList<>();

    List<User> list() {
        return this.users;
    }

    Optional<User> get(final Integer id) {
        return this.users.stream()
                .filter(u -> id.equals(u.getId()))
                .findFirst();
    }

    User add(final User user) {
        user.setId(this.users.size() + 1);
        this.users.add(user);
        return user;
    }

    boolean delete(final Integer id) {
        final Optional<User> userOptional = this.get(id);
        if (userOptional.isPresent()) {
            this.users.remove(userOptional.get());
            return true;
        }
        return false;
    }

    boolean edit(final Integer id, final User user) {
        final Optional<User> userOptional = this.get(id);
        if (userOptional.isPresent()) {
            userOptional.get().setAge(user.getAge());
            userOptional.get().setName(user.getName());
            return true;
        }
        return false;
    }
}
